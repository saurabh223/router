import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

function Product(props){
    return(
        <div className='d-flex justify-content-center'>
            <Card style={{ width: '22rem' }} className='p-2 m-2 text-center ' >
  <Card.Img style={{height:'18rem'}}  variant="top" src={props.imgsrc} />
  <Card.Body>
    <Card.Title>{props.price}</Card.Title>
    <Card.Text>
    {props.desc}
    </Card.Text>
    <Button variant="primary">Add to cart</Button>
  </Card.Body>
</Card>
        </div>
    )
}
export default Product;