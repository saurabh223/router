import React from 'react';
import Container from 'react-bootstrap/Container'
import Carousel from 'react-bootstrap/Carousel'

function Home (){
	return (
        <Container fluid>
<Carousel variant="dark">
  <Carousel.Item>
    <img
      className="d-block w-100" style={{height: '28rem'}}
      src="https://imcbusiness.com/public/product/health-nutrition/Aloe-Vera-v1.png"
      alt="First slide"
    />
    <Carousel.Caption>
      <h5>Alovera </h5>
      <p>Premium Quality alovera products.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100" style={{height: '28rem'}}
      src="https://www.hp.com/content/dam/sites/worldwide/corporate/hp-information/sustainable-impact/planet-product-solutions/1.jpg"
      alt="Second slide"
    />
    <Carousel.Caption>
      <h5>Laptops</h5>
      <p>Buy Beast Performance Laptop</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100" style={{height: '28rem'}}
      src="http://onesciencenutrition.com/wp-content/uploads/2021/11/OSN-family.png"
      alt="Third slide"
    />
    <Carousel.Caption style={{color:"white"}}>
      <h5>Protien</h5>
      <p>International Standard approved brands</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
        </Container>
    )
    
}

export default Home;
