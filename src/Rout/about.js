import React from 'react';
import { Instagram } from 'react-bootstrap-icons/build';
import { Facebook } from 'react-bootstrap-icons/build';
function About () {
	return <div className='text-center'>
		<h2>Follow me on social network!</h2>

		Read more about me : 
		<a href="https://www.instagram.com/saurabh._.ig/" className='about'><Instagram size={26} color="gray" className="m-2"/>
			My insta Id
		</a><br/>
		<a href='facebook.com' className='about'><Facebook size={26} color="gray"/> Facebook</a>
	</div>
}
export default About;
