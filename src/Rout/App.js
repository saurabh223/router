import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from 'react-router-dom';
import Home from './home';
import Contact from './contact';
import About from './about';
import Login from "./login";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Card from './Card'
import './app.css'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="#home">
                <img
                  alt=""
                  src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                />{' '}
                React Bootstrap
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto text-center">
                  <Link to="/" className="menu"><Nav.Link href="#home" >Home</Nav.Link></Link>
                  <Link to="/login" className="menu"><Nav.Link href="#login">Login Here</Nav.Link>
                  </Link>
                  <Link to="/contact" className="menu"><Nav.Link href="#contact">Contact Us</Nav.Link>
                  </Link>
                  <Link to="/social" className="menu"><Nav.Link href="#social">Social connect</Nav.Link>
                  </Link>
                  <Link to="/Product" className="menu"><Nav.Link href="#products">Produts</Nav.Link>
                  </Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
          <Routes>
            <Route exact path='/' element={< Home />}></Route>
            <Route exact path='/social' element={< About />}></Route>
            <Route exact path='/contact' element={< Contact />}></Route>
            <Route exact path='/Login' element={<Login />}></Route>
            <Route exact path='/Product' element={<Card />}></Route>

          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;
