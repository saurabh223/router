import React from "react";
import Product from "./Product";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


function Card(){
    return(
        <div className="container"><Row>
            
            <Col lg={4}>
            <Product imgsrc="https://thumbs.dreamstime.com/b/mockup-iphone-screen-background-have-png-isolated-various-applications-158473491.jpg"
        price="$4"
        desc="Buy Good Phone cases." /></Col>
        <Col lg={4} >
        <Product imgsrc="https://png.pngitem.com/pimgs/s/43-434027_product-beauty-skin-care-personal-care-liquid-tree.png"
        price="$1.99"
        desc="Boston Round Shampoos"/>
        </Col>
        <Col lg={4}>
            <Product imgsrc="https://png.pngitem.com/pimgs/s/160-1606852_png-images-natura-png-transparent-png.png"
            price="$7.99"
            desc="Cosmetic Combo " />
        </Col>
       
        </Row>
        </div>
        
    )
}
export default Card;